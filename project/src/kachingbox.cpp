/* Kaching Box
 *
 * Copyright (c) 2021 waynejohnson.net
 *
 * This software is licensed under the
 * Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * While the license does not allow commercial use, I permit the
 * use (but not sale) of the software commercially.
 * This license will change over time to fit more appropriately.
 *
 */

/**
 * @date started October 2021, finished ---
 * @author sausage@waynejohnsonn.net
 *
 */

#include "orx.h"

#define KEY_DELAY 7
#define MAX_KACHING_TYPE 200


#define MOUSE_ON_NONE 			-1
#define MOUSE_ON_MORE_DRONES 	1
#define MOUSE_ON_LESS_DRONES 	2
#define MOUSE_ON_MORE_INSECTS	3
#define MOUSE_ON_LESS_INSECTS 	4
#define MOUSE_ON_FULLSCREEN 	5
#define MOUSE_ON_BORDERLESS 	6
#define MOUSE_ON_AUTO_TEST	 	7


orxBOOL		isQuitting;
orxS32		droneCount;
orxS32		insectCount;
orxS32		maxDrones;
orxS32		maxInsects;
orxS32		keyDelay;
orxS32		maxMarksAbove60FPS;
orxVECTOR	lastWindowPosition;
orxVECTOR	lastWindowSize;
orxBOOL		isAutoTest;
orxCLOCK	*createObjectsClock;

void PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig)
{
	if (orxObject_GetLastAddedSound(object) != orxNULL){
		orxObject_RemoveSound(object, soundFromConfig);
	}
	orxObject_AddSound(object, soundFromConfig);
}

void KillEnemiesOutsideTheBoundary(){
	for (orxOBJECT *object = orxOBJECT(orxStructure_GetFirst(orxSTRUCTURE_ID_OBJECT));
		 object != orxNULL;
		 object = orxOBJECT(orxStructure_GetNext(object))){

		orxConfig_PushSection(orxObject_GetName(object));
		if (orxConfig_GetBool("IsInsect") || orxConfig_GetBool("IsDrone")){
			orxVECTOR objectPosition;
			orxObject_GetPosition(object, &objectPosition);

			if (objectPosition.fX < 0 || objectPosition.fX > 1920 || objectPosition.fY < -500 || objectPosition.fY > 1080){
				orxObject_SetLifeTime(object, orxFLOAT_0);
			}
		}
		orxConfig_PopSection();
	}
}

void CreateKillerDrone(){
  if (droneCount < maxDrones)  {
	orxObject_CreateFromConfig("DroneObject");
  }
}

void CreateInsect(){
	if (insectCount < maxInsects){
		orxObject_CreateFromConfig("InsectObject");
	}
}

/*
 * Pass in the number of objects out of MAX_KACHING_TYPE,
 * and a recommended scale will be returned.
 * This is support objects becoming smaller
 * the more that are on screen.
 * */
orxVECTOR CalculateObjectScale(int numberOfObject){
	orxFLOAT size = (100 - (orxFLOAT)numberOfObject) / 100;
	if (size < 0.25) //don't let get too small or  
		size = 0.25; //box2d will crash
	
	orxVECTOR newScale = {};
	//orxObject_GetScale(object, &newScale);
	newScale.fX = size;
	newScale.fY = size;
	newScale.fZ = 1.0;

	
	return newScale;
}

orxSTATUS orxFASTCALL ObjectEventHandler(const orxEVENT *_pstEvent){
	if (isQuitting == orxTRUE){
		return orxSTATUS_SUCCESS;
	}

	orxOBJECT *object = orxOBJECT(_pstEvent->hSender);

	if (_pstEvent->eID ==  orxOBJECT_EVENT_CREATE){
		orxConfig_PushSection(orxObject_GetName(object));
		if (orxConfig_GetBool("IsDrone")){
			droneCount++;

			orxVECTOR scale = CalculateObjectScale(maxDrones);
			orxObject_SetScale(object, &scale);
		}
		else if (orxConfig_GetBool("IsInsect")){
			insectCount++;
			
			orxVECTOR scale = CalculateObjectScale(maxInsects);
			orxConfig_PushSection("GrowFXSlot");
			if (orxFLOAT f = orxConfig_GetFloat("EndValue")){
				if (f != scale.fX){
					orxConfig_SetFloat("EndValue", scale.fX);					
				}
			}
			orxConfig_PopSection();
		}
		orxConfig_PopSection();
	}
	else if (_pstEvent->eID ==  orxOBJECT_EVENT_DELETE){
		orxConfig_PushSection(orxObject_GetName(orxOBJECT(_pstEvent->hSender)));
		if (orxConfig_GetBool("IsDrone")){
			droneCount--;
			CreateKillerDrone();
									//orxLOG("!");
		}
		else if (orxConfig_GetBool("IsInsect")){
			insectCount--;
		}
		orxConfig_PopSection();
	}

  return orxSTATUS_SUCCESS;
}

void WriteStringToReport(const orxSTRING string, orxBOOL append){
	orxS32 flags = orxFILE_KU32_FLAG_OPEN_APPEND | orxFILE_KU32_FLAG_OPEN_WRITE;
	if (append == orxFALSE){
		flags =  orxFILE_KU32_FLAG_OPEN_WRITE;
	}

	orxFLOAT width = 0, height = 0;
	orxDisplay_GetScreenSize(&width, &height);

	orxCHAR formattedFileName[30];
    orxString_NPrint(formattedFileName, sizeof(formattedFileName), "fps-report-%dx%d.txt", (int)width, (int)height);
	
	// if a new file - write a header first.
	if (!orxFile_Exists(formattedFileName)){
		orxFILE *file = orxFile_Open (formattedFileName, orxFILE_KU32_FLAG_OPEN_APPEND | orxFILE_KU32_FLAG_OPEN_WRITE | orxFILE_KU32_FLAG_OPEN_WRITE);
		orxFile_Print(file, "Objects\tBodies\tFPS\tFullscreen\n");
		orxFile_Close(file);
	}
	
	orxFILE *file = orxFile_Open (formattedFileName, flags);// ("fps-report.txt",	flags); 	
	orxFile_Print(file, string);
	orxFile_Close(file);
}


void orxFASTCALL AutoBenchmarkingTimerAction(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	//orxLOG("Autoadjusting the parameters.");
	if (!isAutoTest) return;
	
	//Example:
	//58FPS, and maxDrone is 45. 58/60 = 0.96
	//new MaxDrone is 45 * 0.96 = (int)43.2 

	//Example:
	//32FPS, and maxDrone is 90. 32/60 = 0.53
	//new MaxDrone is 90 * 0.53 = (int)47.7 
	
	
	orxU32 fps = orxFPS_GetFPS();
	if (fps >= 60){
		maxDrones += 1;
		maxInsects += 1;
	} else {
		maxDrones = (int)(maxDrones * ((orxFLOAT)orxFPS_GetFPS() / 60.0));
		maxInsects = (int)(maxInsects * ((orxFLOAT)orxFPS_GetFPS() / 60.0));
	}
}

void orxFASTCALL CreateObjectsTimerAction(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	if (droneCount < maxDrones){
		CreateKillerDrone();
	}

	if (insectCount < maxInsects){
		CreateInsect();
	}

	KillEnemiesOutsideTheBoundary();


	orxCHAR row[30];
	orxString_NPrint(row, 29, "%d\t%d\t%d\t%d\n", orxStructure_GetCount(orxSTRUCTURE_ID_OBJECT), orxStructure_GetCount(orxSTRUCTURE_ID_BODY), orxFPS_GetFPS(), orxDisplay_IsFullScreen());

	WriteStringToReport(row, orxTRUE);

}

orxBOOL IsMouseReleased(const orxSTRING input){
	if (orxString_Compare("LeftClick", input) == 0){
		orxBOOL unPressed = orxInput_HasBeenDeactivated(input); 
		return unPressed;
	}
	return orxFALSE;
}

orxBOOL IsInputPressed(const orxSTRING input){
	if (orxInput_HasNewStatus(input)){
		orxBOOL unPressed = orxInput_HasBeenDeactivated(input); 
		if (unPressed == orxTRUE){
			keyDelay = 0;
		}
	}
	orxBOOL pressed = orxInput_IsActive(input);
	return pressed;
}

int GetIdMouseClickedOn(){
	orxVECTOR mouseVector;
	orxMouse_GetPosition(&mouseVector);
	orxRender_GetWorldPosition(&mouseVector, orxNULL, &mouseVector);
	
	orxSTRINGID buttonsID = orxString_GetID("Buttons");
	orxOBJECT *arrowObject = orxObject_Pick(&mouseVector, buttonsID);
	
	if (arrowObject == orxNULL){
		return MOUSE_ON_NONE;
	}
	
	orxObject_AddFX(arrowObject, "ButtonClickFX");
	
	const orxSTRING name = orxObject_GetName(arrowObject);
	if (orxString_Compare(name, "UpArrowObject") == 0){
		return MOUSE_ON_MORE_DRONES;
	}
	if (orxString_Compare(name, "DownArrowObject") == 0){
		return MOUSE_ON_LESS_DRONES;
	}
	if (orxString_Compare(name, "LeftArrowObject") == 0){
		return MOUSE_ON_LESS_INSECTS;
	}
	if (orxString_Compare(name, "RightArrowObject") == 0){
		return MOUSE_ON_MORE_INSECTS;
	}
	if (orxString_Compare(name, "FullScreenLabel") == 0 || orxString_Compare(name, "FullScreenObject") == 0){
		return MOUSE_ON_FULLSCREEN;
	}
	if (orxString_Compare(name, "BorderessLabel") == 0 || orxString_Compare(name, "BorderessObject") == 0){
		return MOUSE_ON_BORDERLESS;
	}
	if (orxString_Compare(name, "AutoBenchmarkLabel") == 0 || orxString_Compare(name, "AutoBenchmarkObject") == 0){
		return MOUSE_ON_AUTO_TEST;
	}
}

void orxFASTCALL InputUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){

	orxBOOL mouseClicked = IsMouseReleased("LeftClick");
	int mouseItem = MOUSE_ON_NONE;
	if (mouseClicked){
		mouseItem = GetIdMouseClickedOn();
	}
	
	if (IsInputPressed("MoreDrones") | mouseItem == MOUSE_ON_MORE_DRONES){
		if (keyDelay == 0){
			maxDrones = orxMIN(MAX_KACHING_TYPE, maxDrones + 1);
			keyDelay = KEY_DELAY;	
		}
	}
	else if (IsInputPressed("LessDrones") | mouseItem == MOUSE_ON_LESS_DRONES){
		if (keyDelay == 0){
			maxDrones = orxMAX(0, maxDrones - 1);
			keyDelay = KEY_DELAY;	
		}

		
	}

	if (IsInputPressed("MoreInsects") | mouseItem == MOUSE_ON_MORE_INSECTS){
		if (keyDelay == 0){
			maxInsects = orxMIN(MAX_KACHING_TYPE, maxInsects + 1);
			keyDelay = KEY_DELAY;	
		}
	}
	else if (IsInputPressed("LessInsects") | mouseItem == MOUSE_ON_LESS_INSECTS){
		if (keyDelay == 0){
			maxInsects = orxMAX(0, maxInsects - 1);
			keyDelay = KEY_DELAY;	
		}
	}
	
	if (orxInput_HasBeenActivated("WindowedFullScreen") | mouseItem == MOUSE_ON_BORDERLESS){
		orxDISPLAY_VIDEO_MODE videoMode;
		orxDisplay_GetVideoMode(orxU32_UNDEFINED, &videoMode);

		if (orxConfig_PushSection("Display")){
			orxBOOL isFullWindow = orxConfig_GetBool("Decoration");
			if (!isFullWindow){ //window going to borderless
				orxVECTOR size = {orxConfig_GetFloat("ScreenWidth"), orxConfig_GetFloat("ScreenHeight")};
				//orxFLOAT w = orxConfig_GetFloat("ScreenWidth");
				//orxFLOAT h = orxConfig_GetFloat("ScreenHeight");
				
				lastWindowSize = size;
				
				orxVECTOR position = {};
				orxConfig_GetVector("ScreenPosition", &position);
				//orxFLOAT w = orxConfig_GetFloat("ScreenWidth");
				//orxFLOAT h = orxConfig_GetFloat("ScreenHeight");
				
				lastWindowPosition = size;
				orxConfig_SetVector("ScreenPosition", &orxVECTOR_0);
			} else { //borderless going to window
				orxConfig_SetFloat("ScreenWidth", lastWindowSize.fX);
				orxConfig_SetFloat("ScreenHeight", lastWindowSize.fY);
				videoMode.u32Width = lastWindowSize.fX;
				videoMode.u32Height = lastWindowSize.fY;

			}
			
			orxConfig_SetBool("Decoration", !isFullWindow);	
			

			

			//orxLOG("w %f h %d", w, h);

			orxConfig_PopSection();
			orxDisplay_SetVideoMode(&videoMode);
			
			orxConfig_SetString("BorderlessString", (isFullWindow == orxTRUE) ? "BORDERLESS OFF" : "BORDERLESS ON");
		}
	}
	
	if (orxInput_HasBeenActivated("ToggleFullScreenMode") | mouseItem == MOUSE_ON_FULLSCREEN){
		//orxDisplay_IsFullScreen()
		orxDisplay_SetFullScreen(!orxDisplay_IsFullScreen());
		orxConfig_SetString("FullScreenString", (orxDisplay_IsFullScreen() == orxFALSE) ? "FULLSCREEN OFF" : "FULLSCREEN ON");
	}
	
	if (orxInput_HasBeenActivated("ToggleAutoTest") | mouseItem == MOUSE_ON_AUTO_TEST){
		isAutoTest = !isAutoTest;
		orxConfig_SetString("AutoBenchmarkString", (isAutoTest == orxFALSE) ? "MANUAL TEST" : "AUTO TEST");
	}
	
	if (keyDelay > 0){
		keyDelay--;
	}
	
}

void EffectAt(const orxVECTOR &position, const orxSTRING effect){
	orxOBJECT *object = orxObject_CreateFromConfig(effect);
	if (object){
		orxObject_SetPosition(object, &position);
	}
}

void BulletFlashAt(const orxVECTOR &position){
	EffectAt(position, "BulletFlash");
}

void ExplosionAt(const orxVECTOR &position){
	EffectAt(position, "ExplosionsObject");
}

orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD){
		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;

		/* Gets colliding objects */
		orxOBJECT *recipient, *sender;
		recipient	= orxOBJECT(_pstEvent->hRecipient);
		sender		= orxOBJECT(_pstEvent->hSender);

		if (recipient != orxNULL && sender != orxNULL){
			struct
			{
			  orxOBJECT *self, *other;
			} configurations[] = {
			  {recipient, sender},
			  {sender, recipient}
			};

			orxBOOL stop = orxFALSE;
			for (orxU32 i = 0; i < orxARRAY_GET_ITEM_COUNT(configurations) && !stop; i++){
				orxOBJECT *self = configurations[i].self;
				orxOBJECT *other = configurations[i].other;

				orxConfig_PushSection(orxObject_GetName(self));

				// Bumper?
				if (orxConfig_GetBool("IsBumper")){
					orxVECTOR upVector = {0.0f, -300.0f, 0.0f};
					orxObject_SetTargetAnim(self, "BumperSpringUpAnim");
					orxObject_ApplyImpulse(other, &upVector, orxNULL);
					PlaySoundOn(self, "Bounce");
					stop = orxTRUE;
				}
				// Bullet?
				else if (orxConfig_GetBool("IsBullet")){
					orxConfig_PushSection(orxObject_GetName(other));

					// With insect?
					if (orxConfig_GetBool("IsInsect")){
						orxVECTOR insectLocation;
						orxObject_GetPosition(other, &insectLocation);
						ExplosionAt(insectLocation);
						orxObject_SetLifeTime(other, orxFLOAT_0);
						CreateInsect();
					}
					// With drone?
					else if (orxConfig_GetBool("IsDrone")){
						orxVECTOR droneLocation;
						orxObject_GetPosition(other, &droneLocation);
						ExplosionAt(droneLocation);
						orxObject_SetLifeTime(other, orxFLOAT_0);
					}
					// With playfield
					else{
						orxVECTOR bulletLocation;
						orxObject_GetPosition(self, &bulletLocation);
						BulletFlashAt(bulletLocation);
					}

					orxObject_SetLifeTime(self, orxFLOAT_0);

					orxConfig_PopSection();
					stop = orxTRUE;
				}

				orxConfig_PopSection();
			}
		}
	}

	return orxSTATUS_SUCCESS;
}
		
/** Run callback for standalone
 */
orxSTATUS orxFASTCALL Run()
{
	orxSTATUS eResult = orxSTATUS_SUCCESS;

	if (orxInput_IsActive("Quit")){
		eResult = orxSTATUS_FAILURE;
	}

	orxConfig_SetS32("ObjectCount", orxStructure_GetCount(orxSTRUCTURE_ID_OBJECT));
	orxConfig_SetU32("FPSCount", orxFPS_GetFPS());
	
	if (orxFPS_GetFPS() >= 60 && orxStructure_GetCount(orxSTRUCTURE_ID_OBJECT) > maxMarksAbove60FPS){
		maxMarksAbove60FPS = orxStructure_GetCount(orxSTRUCTURE_ID_OBJECT);
		orxConfig_SetU32("MarksValue", maxMarksAbove60FPS);
	}
	
	

	orxConfig_SetS32("MaxDrones", maxDrones);
	orxConfig_SetS32("MaxInsects", maxInsects);

	return eResult;
}

void orxFASTCALL Exit()
{
	isQuitting = orxTRUE;
	orxLOG("=================== EXIT ================");
}


orxSTATUS orxFASTCALL Init()
{
	orxViewport_CreateFromConfig("Viewport");
	orxObject_CreateFromConfig("Scene");

	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, ObjectEventHandler);

	//orxClock_AddGlobalTimer(CreateObjectsTimerAction, 0.25, -1, orxNULL);
	createObjectsClock = orxClock_Create(1.0f);
	orxClock_Register(createObjectsClock, CreateObjectsTimerAction, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	
	orxClock_AddGlobalTimer(AutoBenchmarkingTimerAction, 0.1, -1, orxNULL);
	orxClock_Register(orxClock_Get(orxCLOCK_KZ_CORE), InputUpdate, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_HIGH);

	orxClock_SetTickSize(createObjectsClock, 0.1);

	orxConfig_PushSection("Game");

	maxDrones	= orxConfig_GetS32("MaxDrones");
	maxInsects	= orxConfig_GetS32("MaxInsects");

	orxConfig_PopSection();

	droneCount	=
	insectCount	= 0;
	isQuitting	= orxFALSE;
	keyDelay = 0;
	maxMarksAbove60FPS = 0;
	isAutoTest = orxFALSE;

	// All future config queries made here will be in the 'Runtime' section by default
	orxConfig_PushSection("Runtime");


	orxConfig_SetU32("MarksValue", 0);	
	orxConfig_SetString("FullScreenString", "FULLSCREEN OFF");
	orxConfig_SetString("BorderlessString", "BORDERLESS OFF");
	orxConfig_SetString("AutoBenchmarkString", "MANUAL TEST");



	return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL Bootstrap()
{
    // Add config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    // Return orxSTATUS_FAILURE to prevent orx from loading the default config file
    return orxSTATUS_SUCCESS;
}

int main(int argc, char **argv)
{
	orxConfig_SetBootstrap(Bootstrap);
	orx_Execute(argc, argv, Init, Run, Exit);

	return EXIT_SUCCESS;
}


