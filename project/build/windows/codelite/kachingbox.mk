##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug_x64
ProjectName            :=kachingbox
ConfigurationName      :=Debug_x64
WorkspacePath          :=C:/Temp/kachingbox/project/build/windows/codelite
ProjectPath            :=C:/Temp/kachingbox/project/build/windows/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=08/12/2021
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/kachingboxd.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="kachingbox.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/windres.exe
LinkOptions            :=  -static-libgcc -static-libstdc++ -m64 -L/usr/lib64
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ORX)/include $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd 
ArLibs                 :=  "orxd" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/ar.exe rcu
CXX      := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
CC       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/gcc.exe
CXXFLAGS :=  -ffast-math -g -m64 -fno-exceptions $(Preprocessors)
CFLAGS   :=  -ffast-math -g -m64 $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
CC:=x86_64-w64-mingw32-gcc
CXX:=x86_64-w64-mingw32-g++
AR:=x86_64-w64-mingw32-gcc-ar
Objects0=$(IntermediateDirectory)/up_up_up_src_kachingbox.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cmd /c copy /Y C:\Work\Dev\orx\code\lib\dynamic\orx*.dll ..\..\..\bin
	@echo Done

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_up_up_src_kachingbox.cpp$(ObjectSuffix): ../../../src/kachingbox.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_kachingbox.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_kachingbox.cpp$(DependSuffix) -MM ../../../src/kachingbox.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Temp/kachingbox/project/src/kachingbox.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_kachingbox.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_kachingbox.cpp$(PreprocessSuffix): ../../../src/kachingbox.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_kachingbox.cpp$(PreprocessSuffix) ../../../src/kachingbox.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


