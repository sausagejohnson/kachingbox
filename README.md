# README #

![kaching.jpg](https://bitbucket.org/repo/ep6x5M/images/1196941990-kaching.jpg)

### What is Kaching! Box for? ###

This is intended to be a benchmarking program for the Orx Portable Game Engine to measure the FPS when there are many objects, some with bodies and some without, some using various levels of alpha, in order to test on various resolutions and machines.

This will show the programmer to sample Orx's power on a variety of devices, and even prove high frame rates on lower spec machines.

In short, how hard can I push Orx on particular hardware until it dips below 60fps.

It is also intended that the assets and code can be ported to other engines so that Orx can be compared to its competition using typical game development techniques, for example:

* Objects using alpha transparency
* Physics and Revote Joints
* Effects
* Sound

### How do I get set up? ###

.exe's are included.

* There is a premake file and a variety of premade IDE projects if you want to build from scratch.
* Remember to rebuild a new Orx library from scratch if you build this project from scratch.

### Controls  ###

* Up Cursor Key to increase Drones
* Down Cursor Key to decrease Drones
* Right Cursor Key to increase Insects
* Left Cursor Key to decrease Insects
* Esc to quit

### Kaching Marks ###

In a future version, this will be presented as an overall value weighted using a combination of physics, alpha rendered objects, sound and joints.

### Logging ###

In a future version, the overall Kaching Mark score will be broken down into a detailed log that will sample the Kaching! Box, and write the breakdown of how many Frames Per Second you get with:

 - How many total objects. 
 - Of the total: how many alpha objects.
 - Of the total: How many with physics.
 - Of those with physics: how many using a joint.
 - How many sounds are playing.

### What's with the name? ###

The Drones and Insects are two types of Kachings. They are confined to a box. So this is a Kaching! Box.

### Contribution guidelines ###

* Please test or fix however you like.
* If you use another 2D engine apart from Orx, feel free to port and compare!

### Who do I talk to? ###

* Wayne: sausage@waynejohnson.net
* Or visit: https://gitter.im/orx/orx